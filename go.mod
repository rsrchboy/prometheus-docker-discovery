module gitlab.com/rsrchboy/prometheus-docker-discovery

go 1.16

require (
	github.com/docker/docker v20.10.12+incompatible
	github.com/prometheus/client_golang v1.12.2
	github.com/rs/zerolog v1.26.1
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.1
	gitlab.com/rsrchboy/dkrbackoff v0.0.1
)
