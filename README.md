# promdock

This is a small tool to assist in Prometheus service auto-discovery for Docker
containers.  Prometheus has [built-in support for autodiscovery](https://prometheus.io/docs/prometheus/latest/configuration/configuration/),
but unfortunately does not handle Docker directly.  (Docker Swarm, yes, but
not plain-old Docker.)

Promdock handles this by monitoring the [Docker Events API](https://docs.docker.com/engine/api/v1.41/#operation/SystemEvents)
stream for any containers stopping or starting, and then checking their labels
to see if they have a metrics endpoint.  If an endpoint is found, this
information is then made available to Prometheus via [HTTP-SD](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#http_sd_config).

# Demo!

A quick demo is available inside this repository, using
[`docker-compose`](https://github.com/docker/compose).

# Running the discovery service

This service needs read access to the Docker events and containers APIs.  The
use of an API security proxy / shepherd like [ghcr.io/tecnativa/docker-socket-proxy](https://github.com/tecnativa/docker-socket-proxy)
is highly recommended.

Promdock takes connection information from [Docker environment variables](https://pkg.go.dev/github.com/docker/docker/client#FromEnv).
(Want more?  PR's always welcome!)

    $ docker run --rm -it \
        -v /var/run/docker.sock:/var/run/docker.sock \
        registry.gitlab.com/rsrchboy/prometheus-docker-discovery:latest


# Labeling containers correctly

...is driven by labels on the containers themselves.  Note that the container
will be skipped from processing by `promdock` if any required labels are
missing.

| label                         | required | meaning                                                                |
|-------------------------------|----------|------------------------------------------------------------------------|
| `io.weyl.promdock.port`       | yes      | Port to look for metrics on                                            |
| `io.weyl.promdock.network`    | no       | If more than one network attached, the one we should query for metrics |
| `io.weyl.promdock.labels.XXX` | no       | Additional (Prometheus) labels to apply to the target                  |

Networks beginning with `+` will have that prefix rewritten to the expected
name of a compose network.  E.g., say a container is part of a compose project
"moonbase" and `io.weyl.promdock.network` is `+alpha`, the network name Promdock
will expect to find is `moonbase_alpha`.

To attach additional Prometheus labels to a discovered target, use a number of
container labels starting with the `io.weyl.promdock.labels.` prefix.  e.g., a
container with an ip of `1.2.3.4` and labeled as:

    io.weyl.promdock.port: 8080
    io.weyl.promdock.labels.alpha: "one"
    io.weyl.promdock.labels.beta: "two"
    io.weyl.promdock.labels.pizza: "what?!"

...will end up causing a target to be generated like:

    {
        "targets": [
            "1.2.3.4:8080"
        ],
        "labels": {
            "alpha": "one",
            "beta":  "two",
            "pizza": "what?!"
        }
    },

## Launch the discovery container

This example shows launching the service by hand -- extrapolate from there as
you will.

Note that we don't need to run in privileged mode, but we do need access to
the Docker socket to query for information on the containers running.

# LICENSE

Copyright © 2021 Chris Weyl <cweyl@alumni.drew.edu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
