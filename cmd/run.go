/*
Copyright © 2021 Chris Weyl <cweyl@alumni.drew.edu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"net/http"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/rsrchboy/prometheus-docker-discovery/promdock"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run the discovery daemon",
	Long: `This is likely the command you're looking for -- run the daemon,
exposing appropriately labeled containers to Prometheus.

Note that HTTP-SD should be configured to pull from /targets,
while prometheus metrics for _this_ daemon can be found at /metrics.`,
	Run: func(cmd *cobra.Command, args []string) {

		zLevel, err := zerolog.ParseLevel(viper.GetString("loglevel"))
		if err != nil {
			log.Fatal().Err(err).Msg("unrecognized log level")
		}
		zerolog.SetGlobalLevel(zLevel)

		listenAt := viper.GetString("listen")

		log.Info().
			Str("bind-address", listenAt).
			Str("log-level", zLevel.String()).
			Msg("run invoked!")

		promdock.InitialDockerContainerScan()
		go promdock.StartDockerLoop()
		if err := http.ListenAndServe(listenAt, nil); err != nil {
			log.Error().Err(err).Msg("error in http loop!")
		}
	},
}

func init() {
	rootCmd.AddCommand(runCmd)

	// Here you will define your flags and configuration settings.
	// --listen
	runCmd.Flags().String("listen", ":8080", "address we listen on")
	viper.BindPFlag("listen", runCmd.Flags().Lookup("listen"))

	// --loglevel
	runCmd.Flags().String("loglevel", "info", "level of log messages to display")
	viper.BindPFlag("loglevel", runCmd.Flags().Lookup("loglevel"))
}
