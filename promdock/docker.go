/*
Copyright © 2021 Chris Weyl <cweyl@alumni.drew.edu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package promdock

import (
	"context"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/events"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"github.com/rs/zerolog/log"
	"gitlab.com/rsrchboy/dkrbackoff"
)

// dockerClient fetches a... docker client
func dockerClient() (*client.Client, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		// FIXME need error spew ...right?
		log.Error().Err(err).Msg("cannot create a docker client?!")
		return nil, err
	}

	return cli, nil
}

func InitialDockerContainerScan() error {

	// FIXME
	docker, _ := dockerClient()
	ctx := context.Background()

	log.Info().Str("type", "ctr").Str("action", "process-all").Msg("processing objects")
	containers, _ := docker.ContainerList(ctx, types.ContainerListOptions{})

	for _, ctr := range containers {
		msg := wrapEventCtr(ctr)
		processEvent(msg)
	}

	return nil
}

func wrapEventCtr(ctr types.Container) events.Message {
	return events.Message{
		Type:   "container",
		Action: "start",
		Scope:  "local",
		Actor: events.Actor{
			ID:         ctr.ID,
			Attributes: map[string]string{},
		},
	}
}

func StartDockerLoop() error {

	log.Info().Msg("Starting main event loop")

	filter := filters.NewArgs()
	filter.Add("type", "container")
	filter.Add("action", "start")
	filter.Add("action", "stop")

	// we don't actually do anything with this yet, but we may as well start
	// out pointed in a decent direction
	ctx := context.Background()

	op := func(ctx context.Context, d *client.Client) error {
		event, errChan := d.Events(ctx, types.EventsOptions{Filters: filter})
		for {
			select {
			case err := <-errChan:
				log.Error().Err(err).Msg("error returned from docker events stream")
				return err
			case msg := <-event:
				go processEvent(msg)
			}
		}
	}

	if err := dkrbackoff.DockerOp(ctx, op); err != nil {
		log.Error().Err(err).Msg("docker event loop perma-bailed!")
		return err
	}
	return nil
}

func processEvent(msg events.Message) error {

	// native filtering doesn't appear to catch these, so let's ditch a couple
	// high-frequency ones I know we're not going to care about
	if strings.HasPrefix(msg.Action, "exec_") {
		return nil
	}

	finish := eventMetrics(msg.Action)
	defer finish()

	ctx := context.Background()
	elog := log.With().Fields(map[string]interface{}{
		"event-type":     msg.Type,
		"event-action":   msg.Action,
		"event-scope":    msg.Scope,
		"event-actor-id": msg.Actor.ID,
	}).Logger()
	ctx = elog.WithContext(ctx)

	elog.Debug().
		Msg("processing docker event")

	// create does not work -- no network info available
	switch msg.Action {
	case "start":
		addContainerConfig(ctx, msg.Actor.ID)
	case "stop":
		delContainerConfig(ctx, msg.Actor.ID)
	}

	return nil
}
