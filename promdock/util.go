package promdock

import (
	"fmt"
	"strings"
)

func composeRewritePrefix(s, prefix, newPrefix string) string {

	if !strings.HasPrefix(s, prefix) {
		return s
	}

	// e.g. "+default" => "iris_default"
	return fmt.Sprintf("%s_%s", newPrefix, strings.TrimPrefix(s, prefix))
}
