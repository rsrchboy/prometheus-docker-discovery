/*
Copyright © 2021 Chris Weyl <chris@weyl.io>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package promdock

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

const metricsNamespace = "promdock"

var (
	metricsRetriedOps = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: metricsNamespace,
		Subsystem: "docker",
		Name:      "errors_count",
		Help:      "Number of errors encountered handling docker operations.",
	})
	metricsTargetsCount = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: metricsNamespace,
		Subsystem: "prom",
		Name:      "targets_count",
		Help:      "Number of targets we currently expose.",
	})

	metricsEventsCount = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: metricsNamespace,
		Subsystem: "docker",
		Name:      "events_count",
		Help:      "Number of events we're currently handling.",
	}, []string{"action"})
	metricsEventsTotal = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: metricsNamespace,
		Subsystem: "docker",
		Name:      "events_total",
		Help:      "Total number of events we've handled.",
	}, []string{"action"})
	// durations
	metricsEventDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: metricsNamespace,
		Subsystem: "docker",
		Name:      "event_duration",
		Help:      "Time we spend in an event.",
	}, []string{"action"})
)

func eventMetrics(action string) func() {
	metricsEventsCount.WithLabelValues(action).Inc()
	metricsEventsTotal.WithLabelValues(action).Inc()
	timer := prometheus.NewTimer(metricsEventDuration.WithLabelValues(action))
	return func() {
		timer.ObserveDuration()
		metricsEventsCount.WithLabelValues(action).Dec()
	}
}
