/*
Copyright © 2021 Chris Weyl <chris@weyl.io>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package promdock

import (
	"fmt"
	"net/http"

	"gitlab.com/rsrchboy/prometheus-docker-discovery/internal/metrics"
)

var showTargetsHandler = metrics.InstrumentHandler("targets", func(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	res, err := targets.ToJSON()
	if err != nil {
		fmt.Fprint(w, "[]\n")
		return
	}
	fmt.Fprintf(w, "%s\n", *res)
})

func init() {
	http.Handle("/targets", showTargetsHandler)
}
