/*
Copyright © 2021 Chris Weyl <cweyl@alumni.drew.edu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package promdock

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"sync"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/rs/zerolog/log"
	"gitlab.com/rsrchboy/dkrbackoff"
)

// https://prometheus.io/docs/prometheus/latest/http_sd/#http_sd-format

type targetInfo struct {
	Targets     []string          `json:"targets"`
	Labels      map[string]string `json:"labels,omitempty"`
	containerID string
}

type targetInfos []targetInfo

var (
	targets    = targetInfos{}
	targetLock = &sync.RWMutex{}
)

func (infos targetInfos) ToJSON() (*[]byte, error) {
	targetLock.RLock()
	defer func() { targetLock.RUnlock() }()

	b, err := json.Marshal(infos)
	if err != nil {
		return nil, err
	}
	log.Debug().Bytes("json", b).Msg("marshalled targetinfos to json")
	return &b, nil
}

func (infos targetInfos) hasContainer(id string) bool {
	unlock := getTargetsReadLock()
	defer unlock()

	for _, info := range infos {
		if info.containerID == id {
			return true
		}
	}
	return false
}

func getTargetsReadLock() func() {
	targetLock.RLock()
	return func() { targetLock.RUnlock() }
}

func getTargetsWriteLock() func() {
	targetLock.Lock()
	return func() {
		metricsTargetsCount.Set(float64(len(targets)))
		targetLock.Unlock()
	}
}

func addContainerConfig(ctx context.Context, id string) (bool, error) {
	unlock := getTargetsReadLock()
	defer unlock()

	for _, info := range targets {
		if info.containerID == id {
			// found it!
			return false, nil
		}
	}

	go func() {
		writeUnlock := getTargetsWriteLock()
		defer writeUnlock()

		log.Ctx(ctx).Debug().Msg("updating target infos list")
		if info, _ := newInfoFromContainerID(ctx, id); info != nil {
			targets = append(targets, *info)
		}
	}()

	return true, nil
}

const (
	labelPrefix    = "io.weyl.promdock"
	labelPort      = "io.weyl.promdock.port"
	labelNetwork   = "io.weyl.promdock.network"
	labelLabelsPfx = "io.weyl.promdock.labels."

	composeProjectLabel = "com.docker.compose.project"
)

func newInfoFromContainerID(ctx context.Context, id string) (*targetInfo, error) {
	var ctr types.ContainerJSON

	op := func(ctx context.Context, d *client.Client) (err error) {
		ctr, err = d.ContainerInspect(ctx, id)
		return
	}
	if err := dkrbackoff.DockerOp(ctx, op); err != nil {
		log.Ctx(ctx).Error().Err(err).Str("op", "container-inspect").Msg("docker error!")
		return nil, err
	}

	labels := ctr.Config.Labels

	// this may or may not be specified
	netName, hasNetName := labels[labelNetwork]

	if project, ok := labels[composeProjectLabel]; ok && hasNetName {
		netName = composeRewritePrefix(netName, "+", project)
	}

	if port, ok := labels[labelPort]; ok {
		nets := ctr.NetworkSettings.Networks
		if len(nets) == 1 {
			for thisNet, net := range nets {
				address := fmt.Sprintf("%s:%s", net.IPAddress, port)
				log.Ctx(ctx).Info().Str("network", thisNet).Str("address", address).Msg("network found for container")
				return &targetInfo{
					Targets:     []string{address},
					Labels:      buildTargetLabels(labels),
					containerID: id,
				}, nil
			}
		}

		//  # nets != 1 yet no label?
		if !hasNetName {
			log.Ctx(ctx).Warn().Int("network-count", len(ctr.NetworkSettings.Networks)).Bool("network-identified", false).Msg("network not specified")
			return nil, fmt.Errorf("network not specified; %s, %d", id, len(ctr.NetworkSettings.Networks))
		}

		log.Ctx(ctx).Trace().Str("network", netName).Msg("looking for given network")
		net, ok := ctr.NetworkSettings.Networks[netName]
		if !ok {
			log.Ctx(ctx).Warn().Str("network", netName).Bool("found", false).Msg("network specified but not found")
			return nil, fmt.Errorf("network not found; %s, %s", id, netName)
		}
		address := fmt.Sprintf("%s:%s", net.IPAddress, port)
		log.Ctx(ctx).Info().Str("network", netName).Str("address", address).Msg("network found for container")
		return &targetInfo{
			Targets:     []string{address},
			Labels:      buildTargetLabels(labels),
			containerID: id,
		}, nil
	}

	return nil, nil
}

func delContainerConfig(ctx context.Context, id string) (bool, error) {
	unlock := getTargetsReadLock()
	defer unlock()

	if !targets.hasContainer(id) {
		return false, nil
	}

	log.Ctx(ctx).Debug().Msg("destroying any trace of this container")

	go func() {
		writeUnlock := getTargetsWriteLock()
		defer writeUnlock()

		log.Ctx(ctx).Trace().Msg("updating target infos list")

		newTargets := make(targetInfos, len(targets)-1)
		i := 0
		for _, target := range targets {
			if target.containerID != id {
				newTargets[i] = target
				i++
			}
		}
		targets = newTargets
	}()

	return true, nil
}

func buildTargetLabels(labels map[string]string) map[string]string {
	ours := map[string]string{}
	for label, value := range labels {
		if trimmed := strings.TrimPrefix(label, labelLabelsPfx); trimmed != label {
			ours[trimmed] = value
		}
	}
	return ours
}
