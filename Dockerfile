FROM docker.io/library/golang:1.16-alpine3.14 as builder

ADD . /promdock-src
WORKDIR /promdock-src
RUN go mod download
RUN go build -o /promdock

FROM docker.io/library/alpine:3.14

COPY --from=builder --chown=0:0 /promdock /promdock

ENTRYPOINT ["/promdock"]
